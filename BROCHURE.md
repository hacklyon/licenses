# Brochure Partenariat

### Photos
* Lyon vu depuis Fourvière la nuit de la fête des lumières, Photographie par Roger Miret, tous droits réservés
* Flat Icons, Elegant Themes, License GPL
* Vue aérienne du campus LyonTech-la Doua © Devignes conseil - Université de Lyon - 2015

### Modèles
* Investment Firm Brochure, Best InDesign Templates, License Royalty-free

### Polices
* [Entypo](https://www.fontsquirrel.com/fonts/entypo)
